# Misc Tweaks Rimworld

## Description
A bunch of mini mods at this point.

## Installation
Download a release and drop it into your Rimworld mod folder.

## Support
Check issues and get support [here](https://gitlab.com/shaun-yap/misc-tweaks-rimworld/-/issues).

Check out the wiki [here](https://gitlab.com/shaun-yap/misc-tweaks-rimworld/-/wikis/home)

## Roadmap

### Constant
- Any other misc tweaks I want to add into the game.

### 2021 Q4
- ~~Add in unique 'bolt' munitions~~
- Add in the Mk IV Pattern Bolter (Persona Linked)
- Patch more weapon types into the new weapon sorting system; maybe redo it from the ground up so they don't clump up in ranged.
- Add in a super sharp, overpowered melee weapon.

### 2022 Q1
- Add in Tarantula Automated Turrets
- Add in a few more bolter variants, specifically the Stalker Pattern Bolter, the Bolt Pistol, and the Heavy Bolter

## Contributing
I'm currently not accepting contributors. But if you want to jump in, send me a message and we'll see.

## Authors and acknowledgment
It's just me at this current point in time.

## License
This is NOT an open source project. You may maintain it in my absence, but you should not use any part of this for your own mod.

## Project status
Active

## Releases
https://gitlab.com/shaun-yap/misc-tweaks-rimworld/-/releases

